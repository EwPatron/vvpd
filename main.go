package main

import(
	"fmt"
	"vvpd/calc"
)

// Вариант 8, Математика 
func main(){
	var action = 0
	var err error
	for action != 7{
		fmt.Print("\nВыбери действие\n1.Корень уравнения ax + b = 0\n2.Корень уравнения ax² + bx + d = 0\n",
		"3.Корень уравнения ax³ + bx² + cx + d = 0\n4.Найти i-тый член арифм. прогрессии\n",
		"5.Сумма элементов арифм. последовательности\n6.Сумма элементов геом. последовательности\n7.Выход\nДействие: ")
		_, err = fmt.Scanf("%d\n", &action)
		if err != nil{
		 	fmt.Println("Введено не число!")
		 	continue
		}
		switch action{
			case 1:
				calc.ValueOfEquation()
			case 2:
				calc.ValueOfQuadraticEquation()
			case 3:
				calc.ValueOfCubicEquation()
			case 4:
				calc.ArithmeticProgression()
			case 5:
				calc.ArithmeticProgressionSumm()
			case 6:
				calc.GeometricProgressionSumm()
			case 7:
				break
			default:
				fmt.Println("Такого действия нет!")
		}
	}
}

