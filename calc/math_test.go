package calc

import "testing"

func Test_ValueOfEquation(t *testing.T){

	empty_result := ValueOfEquationResult(10, 4)
	if empty_result != -0.4{
		t.Errorf("\nТест линейного уравнения провален, ожидалось %v, получено %v", "-0.4", empty_result)
	} else{
		t.Logf("\nТест линейного уравнения пройден, ожидалось %v, получено %v", "-0.4", empty_result)
	}

	empty_result = ValueOfEquationResult(-5, 14)
	if empty_result != 2.8{
		t.Errorf("\nТест линейного уравнения провален, ожидалось %v, получено %v", "2.8", empty_result)
	} else{
		t.Logf("\nТест линейного уравнения пройден, ожидалось %v, получено %v", "2.8", empty_result)
	}

	empty_result = ValueOfEquationResult(0.5, -10)
	if empty_result != 20{
		t.Errorf("\nТест линейного уравнения провален, ожидалось %v, получено %v", "20", empty_result)
	} else{
		t.Logf("\nТест линейного уравнения пройден, ожидалось %v, получено %v", "20", empty_result)
	}

}

func Test_ValueOfQuadraticEquation(t *testing.T){

	empty_result := ValueOfQuadraticEquationResult(5, 7, 2)
	if (empty_result[0] != -1 && empty_result[1] != -0.4 ){
		t.Errorf("\nТест квадратного уравнения провален, ожидалось %v и %v, получено %v и %v" ,"-1" ,"-0.4",
		 empty_result[0], empty_result[1])
	} else{
		t.Logf("\nТест квадратного уравнения пройден, ожидалось %v и %v, получено %v и %v" ,"-1" ,"-0.4",
		 empty_result[0], empty_result[1])
	}

	empty_result = ValueOfQuadraticEquationResult(1, -11, 18)
	if (empty_result[0] != 2 && empty_result[1] != 9 ){
		t.Errorf("\nТест квадратного уравнения провален, ожидалось %v и %v, получено %v и %v" ,"2" ,"9",
		 empty_result[0], empty_result[1])
	} else{
		t.Logf("\nТест квадратного уравнения пройден, ожидалось %v и %v, получено %v и %v" ,"2" ,"9",
		 empty_result[0], empty_result[1])
	}

	empty_result = ValueOfQuadraticEquationResult(2, 4, 2)
	if (empty_result[0] != -1){
		t.Errorf("\nТест квадратного уравнения провален, ожидалось %v, получено %v" ,"-1" ,
		 empty_result[0])
	} else{
		t.Logf("\nТест квадратного уравнения пройден, ожидалось %v, получено %v" ,"-1" ,
		 empty_result[0])
	}
}

func Test_ValueOfCubicEquation(t *testing.T){

	empty_result := ValueOfCubicEquationResult(1, 2, -1, -2)
	if (empty_result[0] != 1 && empty_result[1] != -1 && empty_result[2] != -2){
		t.Errorf("\nТест кубического уравнения провален, ожидалось %v, %v и %v, получено %v, %v и %v" ,"1" ,"-1", "-2",
		 empty_result[0], empty_result[1], empty_result[2])
	} else{
		t.Logf("\nТест кубического уравнения пройден, ожидалось %v, %v и %v, получено %v, %v и %v" ,"1" ,"-1", "-2",
		 empty_result[0], empty_result[1], empty_result[2])
	}

	empty_result = ValueOfCubicEquationResult(1, -6, 11, -6)
	if (empty_result[0] != 1 && empty_result[1] != 2 && empty_result[2] != 3){
		t.Errorf("\nТест кубического уравнения провален, ожидалось %v, %v и %v, получено %v, %v и %v" ,"1" ,"2", "3",
		 empty_result[0], empty_result[1], empty_result[2])
	} else{
		t.Logf("\nТест кубического уравнения пройден, ожидалось %v, %v и %v, получено %v, %v и %v" ,"1" ,"2", "3",
		 empty_result[0], empty_result[1], empty_result[2])
	}

	empty_result = ValueOfCubicEquationResult(4, 1, -3, -2)
	if (empty_result[0] != 1){
		t.Errorf("\nТест кубического уравнения провален, ожидалось %v, получено %v ","1" ,
		 empty_result[0])
	} else{
		t.Logf("\nТест кубического уравнения пройден, ожидалось %v, получено %v ","1" ,
		 empty_result[0])
	}

}

func Test_ArithmeticProgression(t *testing.T){

	empty_result := ArithmeticProgressionResult(63, 88, 5)
	if empty_result != 415{
		t.Errorf("\nТест поиска элемента арифмитической прогрессии провален, ожидалось %v, получено %v", "415", empty_result)
	} else{
		t.Logf("\nТест поиска элемента арифмитической прогрессии пройден, ожидалось %v, получено %v", "415", empty_result)
	}

	empty_result = ArithmeticProgressionResult(11, 4, 20)
	if empty_result != 87{
		t.Errorf("\nТест поиска элемента арифмитической прогрессии провален, ожидалось %v, получено %v", "87", empty_result)
	} else{
		t.Logf("\nТест поиска элемента арифмитической прогрессии пройден, ожидалось %v, получено %v", "87", empty_result)
	}

	empty_result = ArithmeticProgressionResult(0, -10, 5)
	if empty_result != -40{
		t.Errorf("\nТест поиска элемента арифмитической прогрессии провален, ожидалось %v, получено %v", "-40", empty_result)
	} else{
		t.Logf("\nТест поиска элемента арифмитической прогрессии пройден, ожидалось %v, получено %v", "-40", empty_result)
	}

}

func Test_ArithmeticProgressionSumm(t *testing.T){

	empty_result := ArithmeticProgressionSummResult(0, 5, 10)
	if empty_result != 225{
		t.Errorf("\nТест поиска суммы элементов арифметической прогрессии провален, ожидалось %v, получено %v",
		 "225", empty_result)
	} else{
		t.Logf("\nТест поиска суммы элементов арифметической прогрессии пройден, ожидалось %v, получено %v",
		 "225", empty_result)
	}

	empty_result = ArithmeticProgressionSummResult(9, 71, 6)
	if empty_result != 1119{
		t.Errorf("\nТест поиска суммы элементов арифметической прогрессии провален, ожидалось %v, получено %v",
		 "1119", empty_result)
	} else{
		t.Logf("\nТест поиска суммы элементов арифметической прогрессии пройден, ожидалось %v, получено %v",
		 "1119", empty_result)
	}

	empty_result = ArithmeticProgressionSummResult(-10, -40, 5)
	if empty_result != -450{
		t.Errorf("\nТест поиска суммы элементов арифметической прогрессии провален, ожидалось %v, получено %v",
		 "-450", empty_result)
	} else{
		t.Logf("\nТест поиска суммы элементов арифметической прогрессии пройден, ожидалось %v, получено %v",
		 "-450", empty_result)
	}
}

func Test_GeometricProgressionSumm(t *testing.T){

	empty_result := GeometricProgressionSummResult(1, 4, 5)
	if empty_result != 341{
		t.Errorf("\nТест поиска суммы элементов геометрической прогрессии провален, ожидалось %v, получено %v",
		 "341", empty_result)
	} else{
		t.Logf("\nТест поиска суммы элементов геометрической прогрессии пройден, ожидалось %v, получено %v",
		 "341", empty_result)
	}

	empty_result = GeometricProgressionSummResult(69, 83, 3)
	if empty_result != 481137{
		t.Errorf("\nТест поиска суммы элементов геометрической прогрессии провален, ожидалось %v, получено %v",
		 "481137", empty_result)
	} else{
		t.Logf("\nТест поиска суммы элементов геометрической прогрессии пройден, ожидалось %v, получено %v",
		 "481137", empty_result)
	}

	empty_result = GeometricProgressionSummResult(-10, 5, 6)
	if empty_result != -39060{
		t.Errorf("\nТест поиска суммы элементов геометрической прогрессии провален, ожидалось %v, получено %v",
		 "-39060", empty_result)
	} else{
		t.Logf("\nТест поиска суммы элементов геометрической прогрессии пройден, ожидалось %v, получено %v",
		 "-39060", empty_result)
	}
}