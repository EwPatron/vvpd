package calc

import(
	"fmt"
	"math"
	"math/rand"
	"time"
)

// Корень уравнения ax + b = 0
func ValueOfEquation() error{
	var (
		a float64			// Переменная a, ввод в ручную
		b float64			// Переменная b, ввод в ручную
		x float64			// Корень уравнения, определяется подсчетом
		err error			// Место под ошибочку
	)
	fmt.Print("\nУравнение типа: ax + b = 0\nВведите число a = ")
	_, err = fmt.Scanf("%f\n", &a)
	if err != nil{
		 fmt.Println("Введено не число!")
		 return err
	}
	fmt.Print("\nВведите число b = ")
	_, err = fmt.Scanf("%f\n", &b)
	if err != nil{
		 fmt.Println("Введено не число!")
		 return err
	}
	x = ValueOfEquationResult(a, b)
	fmt.Println("Ответ: x = ", x)
	return nil
}

func ValueOfEquationResult(a float64, b float64	) float64{
	fmt.Println("Уравнение: ", a, "x + ", b, " = 0")
	return -b/a
}

// Корень уравнения ax² + bx + d = 0
func ValueOfQuadraticEquation() error{
	var (
		a float64			// Переменная a, ввод в ручную
		b float64			// Переменная b, ввод в ручную
		d float64			// Переменная d, ввод в ручную
		x []float64			// Результат
		err error			// Большой брат
	)
	fmt.Print("\nУравнение типа: ax² + bx + d = 0\nВведите число a = ")
	_, err = fmt.Scanf("%f\n", &a)
	if err != nil{
		 fmt.Println("Введено не число!")
		 return err
	}
	if (a == 0){
		fmt.Println("a = 0, уравнение не является квадратным!")
		return nil
	}
	fmt.Print("\nВведите число b = ")
	_, err = fmt.Scanf("%f\n", &b)
	if err != nil{
		 fmt.Println("Введено не число!")
		 return err
	}
		fmt.Print("\nВведите число d = ")
	_, err = fmt.Scanf("%f\n", &d)
	if err != nil{
		 fmt.Println("Введено не число!")
		 return err
	}
	x = ValueOfQuadraticEquationResult(a, b, d)
	if (x == nil){
		fmt.Println("Решений нет")
	} else{
		for _, value := range x{
			fmt.Println("Ответ x =", value)
		}
	}
	return nil
}

func ValueOfQuadraticEquationResult(a float64, b float64, d float64) []float64{
	var (
		D float64			// Дискриминант, определяется подсчетом
		x []float64			// Корень уравнения, определяется подсчетом
	)
	fmt.Println("Уравнение: ", a, "x² + ", b, "x + ", d, "= 0")
	D = math.Pow(b,2) - 4*a*d
	if (D > 0){
		x = append(x, (-b-math.Sqrt(D)) / (2*a))
		x = append(x, (-b+math.Sqrt(D)) / (2*a))
	} else if (D == 0){
		x = append(x, (-b) / (2*a))
	} else {
		return nil
	}
	return x
}
// Корень уравнения ax³ + bx² + cx + d = 0
// Версия по решению от какого-то китайца, крашется из-за -3 под корнем (похоже китаец был ненастоящий)
func ValueOfCubicEquation() error{
	var (
		a int			// Переменная a, ввод в ручную
		b int			// Переменная b, ввод в ручную
		c int			// Переменная с, ввод в ручную
		d int			// Переменная d, ввод в ручную
		x []float64			// Корень уравнения, определяется подсчетом
		err error			// Мой лучший друг
	)
	fmt.Print("\nУравнение типа: ax³ + bx² + cx + d = 0\nВведите число a = ")
	_, err = fmt.Scanf("%d\n", &a)
	if err != nil{
		 fmt.Println("Введено не число!")
		 return err
	}
	if (a == 0){
		fmt.Println("a = 0, уравнение не является кубическим!")
		return nil
	}
	fmt.Print("\nВведите число b = ")
	_, err = fmt.Scanf("%d\n", &b)
	if err != nil{
		 fmt.Println("Введено не число!")
		 return err
	}
	fmt.Print("\nВведите число c = ")
	_, err = fmt.Scanf("%d\n", &c)
	if err != nil{
		fmt.Println("Введено не число!")
		 return err
	}
	fmt.Print("\nВведите число d = ")
	_, err = fmt.Scanf("%d\n", &d)
	if err != nil{
		 fmt.Println("Введено не число!")
		 return err
	}
	if (d == 0){
		fmt.Println("d = 0, пока что я не могу считать при d = 0")
		return nil
	}
	x = ValueOfCubicEquationResult(a,b,c,d)
	if (x == nil){
		fmt.Println("Решений нет")
	} else{
		for _, value := range x{
			fmt.Println("Ответ x =", value)
		}
	}
	return nil
}

func ValueOfCubicEquationResult(a int, b int, c int,  d int) []float64{
	var (
		a_div []int			// все делители числа a
		d_div []int			// все делители числа d
		a_d_div []float64	// деление числел a_div и d_div
		x []float64			// Корень уравнения, определяется подсчетом
	)
	fmt.Println("Уравнение: ", a, "x³ + ", b, "x² + ", c, "х + ", d, " = 0")
	a_div = DivisorOfNumber(int(math.Abs(float64(a))))
	d_div = DivisorOfNumber(int(math.Abs(float64(d))))
	for _, a_value := range a_div{
		a_d_div = append(a_d_div, float64(a_value))
		a_d_div = append(a_d_div, float64(-a_value))
		for _, d_value := range d_div{
			a_d_div = append(a_d_div, float64(a_value)/float64(d_value))
			a_d_div = append(a_d_div, float64(-a_value)/float64(d_value))
		}
	}
	for _, d_value := range d_div{
		a_d_div = append(a_d_div, float64(d_value))
		a_d_div = append(a_d_div, float64(-d_value))
	}
	a_d_div = RemoveDuplicate(a_d_div)
	for _, a_d_value := range a_d_div{
		if(float64(a)*math.Pow(a_d_value,3)+float64(b)*math.Pow(a_d_value,2)+float64(c)*a_d_value+float64(d) == 0){
			x = append(x, a_d_value)
		}
	}
	x = RemoveDuplicate(x)
	return x
}

// Находим все делители числа (для вычисления куб. корня)
func DivisorOfNumber(number int)[]int{
	var number_div []int
	for  i := 1; i <= number; i++{
		if (number%i==0){
			number_div = append(number_div, i)
		}
	}
	return number_div
}

// Удаление дублей из массива
func RemoveDuplicate(intSlice []float64) []float64{
	keys := make(map[float64]bool)
    list := []float64{}	
    for _, entry := range intSlice {
        if _, value := keys[entry]; !value {
            keys[entry] = true
            list = append(list, entry)
        }
    }
	return list    
}

// нахождение i-ого члена арифмитической прогессии
func ArithmeticProgression() error{
	var (
		step float64		// Шаг прогрессии
		start float64		// Начальное число прогрессии
		numberForSearch	int	// Номер переменной последовательности для поиска
		err error			// Место под ошибочку
		action int			// Действие пользователя
	)

	fmt.Print("\nНахождение i-ого члена арифм. прогрессии, выберите действие:\n1.Взять значения прогресси рандомно\n",
	"2.Указать параметры прогрессии\nДействие:")
	_, err = fmt.Scanf("%d\n", &action)
	if err != nil{
		fmt.Println("Введено не число!")
	}
	switch action{
		case 1:
			generator := rand.New(rand.NewSource(time.Now().UnixNano()))
			start = float64(generator.Intn(100))
			step = float64(generator.Intn(100))
			fmt.Println("Начало прогрессии: ", start, "\nШаг прогрессии: ", step)
		case 2:
			fmt.Print("\nВведите начальное значение прогрессии = ")
			_, err = fmt.Scanf("%f\n", &start)
			if err != nil{
				fmt.Println("Введено не число!")
				return err
			}
			fmt.Print("\nВведите шаг прогрессии = ")
			_, err = fmt.Scanf("%f\n", &step)
			if err != nil{
				fmt.Println("Введено не число!")
				return err
			}
			if step == 0{
				fmt.Println("Необычное решение, ну пусть будет прогрессия с шагом 0")
			}
		default:
			fmt.Println("Такого действия нет!")
			return nil
		}

		fmt.Print("\nВведите номер члена арифмитической последовательности = ")
	_, err = fmt.Scanf("%d\n", &numberForSearch)
	if err != nil{
		fmt.Println("Введено не целое число!")
		 return err
	}
	if numberForSearch < 1{
		fmt.Println("Номер члена последовательности не может быть меньше 1 !")
		return nil
	}
	fmt.Println(start, ", ", step, ", ", numberForSearch)
	start = ArithmeticProgressionResult(start, step,  numberForSearch)
	fmt.Println(numberForSearch, " член последовательности равен ", start)
	return nil
}

// нахождение i-ого члена арифмитической прогессии
func ArithmeticProgressionResult(start float64, step float64,  numberForSearch int) float64{
	for  i := 1; i < numberForSearch; i++{
		start = start + step
	}
	return start
}

// нахождение суммы  арифмитической прогессии
func ArithmeticProgressionSumm() error{
	var (
		step float64		// Шаг прогрессии
		start float64		// Начальное число прогрессии
		sum	float64			// Номер переменной последовательности для поиска
		err error			// Бро
		action int			// Действие пользователя
		totalElement int	// Все кол-во членов прогрессии
	)

	fmt.Print("\nНахождение i-ого члена арифм. прогрессии, выберите действие:\n1.Взять значения прогресси рандомно\n",
	"2.Указать параметры прогрессии\nДействие:")
	_, err = fmt.Scanf("%d\n", &action)
	if err != nil{
		fmt.Println("Введено не число!")
	}
	switch action{
		case 1:
			generator := rand.New(rand.NewSource(time.Now().UnixNano()))
			start = float64(generator.Intn(100))
			step = float64(generator.Intn(100))
			fmt.Println("Начало прогрессии: ", start, "\nШаг прогрессии: ", step)
		case 2:
			fmt.Print("\nВведите начальное значение прогрессии = ")
			_, err = fmt.Scanf("%f\n", &start)
			if err != nil{
				fmt.Println("Введено не число!")
				return err
			}
			fmt.Print("\nВведите шаг прогрессии = ")
			_, err = fmt.Scanf("%f\n", &step)
			if err != nil{
				fmt.Println("Введено не число!")
				return err
			}
			if step == 0{
				fmt.Println("Необычное решение, ну пусть будет прогрессия с шагом 0")
			}
		default:
			fmt.Println("Такого действия нет!")
			return nil
		}

	fmt.Print("\nВведите кол-во членов арифмитической последовательности = ")
	_, err = fmt.Scanf("%d\n", &totalElement)
	if err != nil{
		fmt.Println("Введено не целое число!")
		 return err
	}
	if totalElement < 1{
		fmt.Println("Членов последовательности не может быть меньше 1 !")
		return nil
	}
	sum = ArithmeticProgressionSummResult(start, step, totalElement)

	fmt.Println("Сумма членов последовательности равна: ", sum)
	return nil
}

func ArithmeticProgressionSummResult(start float64, step float64, totalElement int) float64{

	var sum	float64			// Сумма

	sum = start
	for  i := 1; i < totalElement; i++{
		start = start + step
		sum = sum + start
	}
	return sum
}

// нахождение суммы  геометрической прогессии
func GeometricProgressionSumm() error{
	var (
		step float64		// Шаг прогрессии
		start float64		// Начальное число прогрессии
		sum	float64			// Номер переменной последовательности для поиска
		err error			// На страже порядка
		action int			// Действие пользователя
		totalElement int	// Все кол-во членов прогрессии
	)

	fmt.Print("\nНахождение i-ого члена геом.. прогрессии, выберите действие:\n1.Взять значения прогресси рандомно\n",
	"2.Указать параметры прогрессии\nДействие:")
	_, err = fmt.Scanf("%d\n", &action)
	if err != nil{
		fmt.Println("Введено не число!")
	}
	switch action{
		case 1:
			generator := rand.New(rand.NewSource(time.Now().UnixNano()))
			start = float64(generator.Intn(100))
			step = float64(generator.Intn(100))
			fmt.Println("Начало прогрессии: ", start, "\nШаг прогрессии: ", step)
		case 2:
			fmt.Print("\nВведите начальное значение прогрессии = ")
			_, err = fmt.Scanf("%f\n", &start)
			if err != nil{
				fmt.Println("Введено не число!")
				return err
			}
			fmt.Print("\nВведите шаг прогрессии = ")
			_, err = fmt.Scanf("%f\n", &step)
			if err != nil{
				fmt.Println("Введено не число!")
				return err
			}
			if step == 0{
				fmt.Println("Необычное решение, ну пусть будет прогрессия с шагом 0")
			}
		default:
			fmt.Println("Такого действия нет!")
			return nil
		}

	fmt.Print("\nВведите кол-во членов геометрической последовательности = ")
	_, err = fmt.Scanf("%d\n", &totalElement)
	if err != nil{
		fmt.Println("Введено не целое число!")
		 return err
	}
	if totalElement < 1{
		fmt.Println("Членов последовательности не может быть меньше 1 !")
		return nil
	}
	sum = GeometricProgressionSummResult(start, step, totalElement)

	fmt.Println("Сумма членов последовательности равна: ", sum)
	return nil
}

func GeometricProgressionSummResult(start float64, step float64, totalElement int) float64{
	var sum	float64			// Номер переменной последовательности для поиска
	sum = start
	for  i := 1; i < totalElement; i++{
		start = start * step
		sum = sum + start
	}
	return sum
}